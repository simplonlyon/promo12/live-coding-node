const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const firstRouter = require('./controllers/first-controller');
const dogRouter = require('./controllers/dog-controller');

const app = express();
const port = process.env.PORT | 4000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());

app.use('/first', firstRouter);
app.use('/dog', dogRouter);


app.get('/', (req,res) => {

    res.end('<p>Welcome to this node.js application</p>');
});

app.listen(port, () => {
    console.log('App is listening on port 4000');
});


