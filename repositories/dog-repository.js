const mysql = require('mysql2/promise');
const Dog = require('../entities/dog');

const connection = mysql.createPool("mysql://root:1234@localhost:3306/live_coding_node");
/**
 * @returns {Promise<Dog[]>}
 */
async function findAll() {
    
    const [results, fields] = await connection.execute('SELECT * FROM dog');

    return results.map(item => new Dog(item['name'], item['breed'], item['birthdate'], item['id']));



}
/**
 * @param {Dog} dog 
 */
async function add(dog) {
    const [results, fields]= await connection.execute('INSERT INTO dog (name,breed,birthdate) VALUES (?,?,?)', [dog.name, dog.breed, dog.birthdate]);
    dog.id = results.insertId;
    return dog;
}


module.exports = {
    findAll,
    add
};