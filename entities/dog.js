

class Dog {
    constructor(name = '', breed = '', birthdate = new Date(), id = null){
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
}

module.exports = Dog;