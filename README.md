# Première Scéance de Live Coding sur node
Un petit projet node.js avec express pour faire une API Rest et mysql pour faire persister les données.

Il y a la version avec callback sur la branche callback, et là une version avec promise sur la master.

## Exercice
Terminer le dog-repository.js en rajoutant les function find(id), update(dog) et delete(dog) et du coup terminer également le dog-controller en rajoutant les routes qui vont avec. (je mettrai la correction sur une autre branche)