const {Router} = require('express');

const dogRouter = Router();

const repo = require('../repositories/dog-repository');


dogRouter.get('/', async (req,res) => {
    const dogs = await repo.findAll();
    res.json(dogs);
});

dogRouter.post('/', async (req,res) => {
    
    repo.add(req.body).then(data => res.status(201).json(data));
});

module.exports = dogRouter;